using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CORE.Models;
using Newtonsoft.Json;

namespace CORE.Controllers
{
    public class AppointmentController
    {
        private string _filePath;
        private List<AppointmentModel> _appointments;

        #region Constructors

        public AppointmentController()
        {
            _filePath = "appointment.json";
            Load();
        }

        public AppointmentController(String filePath)
        {
            _filePath = filePath;
            Load();
        }

        #endregion

        #region Create

        public void Create(AppointmentModel appointment)
        {
            if (_appointments != null)
            {
                if (_appointments.Count > 0)
                {
                    appointment.Id = _appointments[_appointments.Count - 1].Id + 1;
                }
                else
                {
                    appointment.Id = 1;
                }
            }
            else
            {
                appointment.Id = 1;
                _appointments = new List<AppointmentModel>();
            }


            DateTime dateToCheck = appointment.Date;
            DateTime currentDate = DateTime.Now;

            if (currentDate >= dateToCheck)
            {
                appointment.Past = true;
            }
            else
            {
                appointment.Past = false;
            }


            _appointments.Add(appointment);
            Save();
        }

        #endregion

        #region Get

        public List<AppointmentModel> GetAll()
        {
            if (_appointments  == null)
            {
                return new List<AppointmentModel>();
            }
            return _appointments;
        }
        
        public List<AppointmentModel> GetPast()
        {
            return _appointments.FindAll(appointment => appointment.Date <= DateTime.Now);
        }
        public List<AppointmentModel> GetFirst()
        {
            return _appointments.FindAll(appointment => appointment.Date >= DateTime.Now);
        }
        public AppointmentModel GetById(int id)
        {
            return _appointments.Find(appointment => appointment.Id == id);
        }

        #endregion

        #region Remove

        public void Remove(int id)
        {
            _appointments.Remove(_appointments.Find(doctor => doctor.Id == id));

            Save();
        }

        #endregion

        #region Set

        public void SetReportById(int id, string reportText)
        {
            var appointment = _appointments.Find(appointmen => appointmen.Id == id);
            appointment.Report = reportText;
            _appointments[_appointments.IndexOf(appointment)] = appointment;
            Save();
        }

        public void AddMedicalHistoryById(int id, string recording)
        {
            AppointmentModel appointment = _appointments.Find(a => a.Id == id);
            if (appointment != null)
            {
                if (appointment.MedicalHistory == null)
                {
                    appointment.MedicalHistory = new List<string>();
                }

                appointment.MedicalHistory.Add(recording);
                Save();
            }
        }

        #endregion

        #region SAVE-LOAD

        public void Save()
        {
            string json = JsonConvert.SerializeObject(_appointments);
            File.WriteAllText(_filePath, json);
        }

        public void Load()
        {
            if (!File.Exists(_filePath))
            {
                List<AppointmentModel> doctors = new List<AppointmentModel>();
                _appointments = doctors;
            }

            try
            {
                string json = File.ReadAllText(_filePath);
                List<AppointmentModel> appointments = JsonConvert.DeserializeObject<List<AppointmentModel>>(json);
                for (int i = 0; i < appointments.Count; i++)
                {
                    DateTime dateToCheck = appointments[i].Date;
                    DateTime currentDate = DateTime.Now;

                    if (currentDate >= dateToCheck)
                    {
                        appointments[i].Past = true;
                    }
                    else
                    {
                        appointments[i].Past = false;
                    }
                }

                _appointments = appointments;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error loading appointments from file: {ex.Message}");
            }
        }

        #endregion
    }
}