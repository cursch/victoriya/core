using System;
using System.Collections.Generic;
using CORE.Controllers;

namespace CORE.Models
{
    public class AppointmentModel
    {
        private PatientController _patients = new PatientController();
        public int Id { get; set; }
        public int DoctorId { get; set; }
        public int PatientId { get; set; }
        public DateTime Date { get; set; }
        public string Report { get; set; }
        public List<string> MedicalHistory { get; set; }
        public bool Past { get; set; } = false;
        
        public string FullName
        {
            get { return $" {_patients.GetById(PatientId).FullName} | {Date} | {Report} "; }
        }
    }
}