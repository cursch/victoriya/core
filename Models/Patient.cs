namespace CORE.Models
{
    public class PatientModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }

        public string FullName
        {
            get { return $"{LastName} {FirstName} {MiddleName}"; }
        }

        public string PhoneNumber { get; set; }
    }
}