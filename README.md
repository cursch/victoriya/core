# Описание Контроллеров
## Классы AppointmentController, DoctorController и PatientController предоставляют набор методов для работы с соответствующими данными (записи на прием, докторы, пациенты).

### Общие методы для всех трех классов:
- Конструкторы (public AppointmentController(), public DoctorController(), public PatientController(String filePath)): инициализируют класс, устанавливают путь к файлу с данными и загружают данные из файла в список соответствующих объектов.
- Save(): сохраняет текущие данные из списка объектов в файл в формате JSON.
- Load(): загружает данные из файла в формате JSON и перезаписывает список объектов в соответствии с полученными данными.

### Методы класса AppointmentController:
- Create(AppointmentModel appointment): создает новую запись на прием, генерирует идентификатор, устанавливает флаг, указывающий на то, прошел ли прием, добавляет запись в список объектов и сохраняет изменения в файле.
- GetAll(): возвращает список всех записей на прием из списка объектов.
- GetPast(): возвращает список записей на прием, которые уже прошли на текущую дату.
- GetFirst(): возвращает список записей на прием, которые будут проходить после текущей даты.
- GetById(int id): возвращает запись на прием с указанным идентификатором.
- Remove(int id): удаляет запись на прием с указанным идентификатором из списка объектов и сохраняет изменения в файле.
- SetReportById(int id, string reportText): устанавливает отчет по записи на прием с указанным идентификатором.
- AddMedicalHistoryById(int id, string recording): добавляет запись в медицинскую историю записи на прием с указанным идентификатором.

### Методы класса DoctorController:
- Create(DoctorModel doctor): создает новый объект доктора, генерирует идентификатор, добавляет объект в список и сохраняет изменения в файле.
- GetAll(): возвращает список всех объектов докторов из списка объектов.
- GetById(int id): возвращает объект доктора с указанным идентификатором.
- Remove(int id): удаляет объект доктора с указанным идентификатором из списка объектов и сохраняет изменения в файле.

### Методы класса PatientController:
- Create(PatientModel patient): создает новый объект пациента, генерирует идентификатор, добавляет объект в список и сохраняет изменения в файле.
- GetAll(): возвращает список всех объектов пациентов из списка объектов.
- GetById(int id): возвращает объект пациента с указанным идентификатором.
- Remove(int id): удаляет объект пациента с указанным идентификатором из списка объектов и сохраняет изменения в файле.

# Описание Моделей

## класс AppointmentModel. Класс содержит поля и методы для представления записи на прием.

### Поля класса AppointmentModel:

- Id: идентификатор записи на прием.
- DoctorId: идентификатор доктора, к которому записан пациент.
- PatientId: идентификатор пациента, записанного на прием.
- Date: дата и время записи на прием.
- Report: отчет о приеме.
- MedicalHistory: список записей медицинской истории для данной записи на прием.
- Past: флаг, указывающий на то, прошел ли прием.

### Методы класса AppointmentModel:

- FullName: возвращает строку с полным именем пациента, датой приема и отчетом о приеме.

### Класс AppointmentModel использует объект PatientController для получения полного имени пациента, записанного на прием, и других данных о пациенте.


## класс DoctorModel. Класс содержит поля и методы для представления доктора.

### Поля класса DoctorModel:

- Id: идентификатор доктора.
- FirstName: имя доктора.
- LastName: фамилия доктора.
- MiddleName: отчество доктора.
- Specialization: специализация доктора.

### Методы класса DoctorModel:
- FullName: возвращает полное имя доктора в формате "Фамилия Имя Отчество".


## класс PatientModel. Класс содержит поля и методы для представления пациента.

### Поля класса PatientModel:

- Id: идентификатор пациента.
- FirstName: имя пациента.
- LastName: фамилия пациента.
- MiddleName: отчество пациента.
- PhoneNumber: номер телефона пациента.

### Методы класса PatientModel:

- FullName: возвращает полное имя пациента в формате "Фамилия Имя Отчество".

# ОПисание для презентации
## Модель Пациента
namespace CORE.Models
```
    public class PatientModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }

        public string FullName
        {
            get { return $"{LastName} {FirstName} {MiddleName}"; }
        }

        public string PhoneNumber { get; set; }
    }
```

## модель Врача
```
namespace CORE.Models
{
    public class DoctorModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string Specialization { get; set; }

        public string FullName
        {
            get { return $"{LastName} {FirstName} {MiddleName}"; }
        }
    }
}
```

## Модель Записи ко врачу
```
namespace CORE.Models
{
    public class AppointmentModel
    {
        private PatientController _patients = new PatientController();
        public int Id { get; set; }
        public int DoctorId { get; set; }
        public int PatientId { get; set; }
        public DateTime Date { get; set; }
        public string Report { get; set; }
        public List<string> MedicalHistory { get; set; }
        public bool Past { get; set; } = false;
        
        public string FullName
        {
            get { return $" {_patients.GetById(PatientId).FullName} | {Date} | {Report} "; }
        }
    }
}
```

## Контроллер доктора
```
public class DoctorController
    {
        private string _filePath;
        private List<DoctorModel> _doctors;

        #region Constructors

        public DoctorController()
        {
            _filePath = "doctor.json";
            Load();
        }

        public DoctorController(String filePath)
        {
            _filePath = filePath;
            Load();
        }

        #endregion

        #region Create

        public void Create(DoctorModel doctor)
        {
            if (_doctors != null)
            {
                if (_doctors.Count > 0)
                {
                    doctor.Id = _doctors[_doctors.Count - 1].Id + 1;
                }
                else
                {
                    doctor.Id = 1;
                }
            }
            else
            {
                doctor.Id = 1;
                _doctors = new List<DoctorModel>();
            }

            _doctors.Add(doctor);
            Save();
        }

        #endregion

        #region Get

        public List<DoctorModel> GetAll()
        {
            return _doctors;
        }

        public DoctorModel GetById(int id)
        {
            return _doctors.Find(doctor => doctor.Id == id);
        }

        #endregion

        #region Remove

        public void Remove(int id)
        {
            _doctors.Remove(_doctors.Find(doctor => doctor.Id == id));
            
            Save();
        }

        #endregion
        
        #region SAVE-LOAD

        public void Save()
        {
            string json = JsonConvert.SerializeObject(_doctors);
            File.WriteAllText(_filePath, json);
        }

        public void Load()
        {
            if (!File.Exists(_filePath))
            {
                List<DoctorModel> doctors = new List<DoctorModel>();
                _doctors = doctors;
            }

            try
            {
                string json = File.ReadAllText(_filePath);
                List<DoctorModel> doctors = JsonConvert.DeserializeObject<List<DoctorModel>>(json);
                _doctors = doctors;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error loading doctors from file: {ex.Message}");
            }
        }

        #endregion
    }
```

## Контроллер Пациента
```
public class PatientController
    {
        private string _filePath;
        private List<PatientModel> _patient;

        #region Constructors

        public PatientController()
        {
            _filePath = "patient.json";
            Load();
        }

        public PatientController(String filePath)
        {
            _filePath = filePath;
            Load();
        }

        #endregion

        #region Create

        public void Create(PatientModel patient)
        {
            if (_patient != null)
            {
                if (_patient.Count > 0)
                {
                    patient.Id = _patient[_patient.Count - 1].Id + 1;
                }
                else
                {
                    patient.Id = 1;
                }
            }
            else
            {
                patient.Id = 1;
                _patient = new List<PatientModel>();
            }

            _patient.Add(patient);
            Save();
        }

        #endregion

        #region Get

        public List<PatientModel> GetAll()
        {
            return _patient;
        }

        public PatientModel GetById(int id)
        {
            return _patient.Find(patient => patient.Id == id);
        }

        #endregion

        #region Remove

        public void Remove(int id)
        {
            _patient.Remove(_patient.Find(doctor => doctor.Id == id));

            Save();
        }

        #endregion

        #region SAVE-LOAD

        public void Save()
        {
            string json = JsonConvert.SerializeObject(_patient);
            File.WriteAllText(_filePath, json);
        }

        public void Load()
        {
            if (!File.Exists(_filePath))
            {
                List<PatientModel> doctors = new List<PatientModel>();
                _patient = doctors;
            }

            try
            {
                string json = File.ReadAllText(_filePath);
                List<PatientModel> patients = JsonConvert.DeserializeObject<List<PatientModel>>(json);
                _patient = patients;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error loading patient from file: {ex.Message}");
            }
        }

        #endregion
    }
```

## Контроллер Записей ко врачу
```
public class AppointmentController
    {
        private string _filePath;
        private List<AppointmentModel> _appointments;

        #region Constructors

        public AppointmentController()
        {
            _filePath = "appointment.json";
            Load();
        }

        public AppointmentController(String filePath)
        {
            _filePath = filePath;
            Load();
        }

        #endregion

        #region Create

        public void Create(AppointmentModel appointment)
        {
            if (_appointments != null)
            {
                if (_appointments.Count > 0)
                {
                    appointment.Id = _appointments[_appointments.Count - 1].Id + 1;
                }
                else
                {
                    appointment.Id = 1;
                }
            }
            else
            {
                appointment.Id = 1;
                _appointments = new List<AppointmentModel>();
            }


            DateTime dateToCheck = appointment.Date;
            DateTime currentDate = DateTime.Now;

            if (currentDate >= dateToCheck)
            {
                appointment.Past = true;
            }
            else
            {
                appointment.Past = false;
            }


            _appointments.Add(appointment);
            Save();
        }

        #endregion

        #region Get

        public List<AppointmentModel> GetAll()
        {
            if (_appointments  == null)
            {
                return new List<AppointmentModel>();
            }
            return _appointments;
        }
        
        public List<AppointmentModel> GetPast()
        {
            return _appointments.FindAll(appointment => appointment.Date <= DateTime.Now);
        }
        public List<AppointmentModel> GetFirst()
        {
            return _appointments.FindAll(appointment => appointment.Date >= DateTime.Now);
        }
        public AppointmentModel GetById(int id)
        {
            return _appointments.Find(appointment => appointment.Id == id);
        }

        #endregion

        #region Remove

        public void Remove(int id)
        {
            _appointments.Remove(_appointments.Find(doctor => doctor.Id == id));

            Save();
        }

        #endregion

        #region Set

        public void SetReportById(int id, string reportText)
        {
            var appointment = _appointments.Find(appointmen => appointmen.Id == id);
            appointment.Report = reportText;
            _appointments[_appointments.IndexOf(appointment)] = appointment;
            Save();
        }

        public void AddMedicalHistoryById(int id, string recording)
        {
            AppointmentModel appointment = _appointments.Find(a => a.Id == id);
            if (appointment != null)
            {
                if (appointment.MedicalHistory == null)
                {
                    appointment.MedicalHistory = new List<string>();
                }

                appointment.MedicalHistory.Add(recording);
                Save();
            }
        }

        #endregion

        #region SAVE-LOAD

        public void Save()
        {
            string json = JsonConvert.SerializeObject(_appointments);
            File.WriteAllText(_filePath, json);
        }

        public void Load()
        {
            if (!File.Exists(_filePath))
            {
                List<AppointmentModel> doctors = new List<AppointmentModel>();
                _appointments = doctors;
            }

            try
            {
                string json = File.ReadAllText(_filePath);
                List<AppointmentModel> appointments = JsonConvert.DeserializeObject<List<AppointmentModel>>(json);
                for (int i = 0; i < appointments.Count; i++)
                {
                    DateTime dateToCheck = appointments[i].Date;
                    DateTime currentDate = DateTime.Now;

                    if (currentDate >= dateToCheck)
                    {
                        appointments[i].Past = true;
                    }
                    else
                    {
                        appointments[i].Past = false;
                    }
                }

                _appointments = appointments;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error loading appointments from file: {ex.Message}");
            }
        }

        #endregion
    }
```
